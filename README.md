# File renamer

## Description

**refi** is Rust program to help you rename files fast in numeric order.

This was previously named as **img-renamer**. All versions >= 3.0.0 are for **refi**, below 3.0.0 are for **img-renamer**

## Installation

You can install **refi** from [crates.io](https://crates.io/crates/refi) or by [building yourself from source.](https://gitlab.com/miicat/refi/-/wikis/installation#build-from-source)

## Usage

See [wiki](https://gitlab.com/miicat/refi/-/wikis/) for full usage

### Basic usage

This will rename all files in `~/Pictures` folder like this: `0000001.png`, `0000002.jpg`, `0000003.txt`...

`refi ~/Pictures`  
Output:

```text
image.png -> 0000001.png
picture.jpg -> 0000002.jpg
file.txt -> 0000003.txt
____________________
Renaming 3 of 3
Are you sure you want to rename (y/n): y
Do you want to save a log file(y/n): n
```

### Print help text

Simply run `refi -h` or `refi --help` for long help.

## Author

[Miika Launiainen](https://gitlab.com/miicat)

## Donating

If you want to support me, feel free to use paypal
[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://paypal.me/miicat)

## License

This library is licensed under GPLv3

use clap::{builder::PossibleValue, crate_version, value_parser, Arg, ArgAction, Command};
use refi::LogLevel;

fn main() {
    let matches = Command::new("refi")
        .version(crate_version!())
        .author("Miika Launiainen <miika@miicat.eu>")
        .about("refi is a command line tool to rename files in numeric order.")
        .arg(
            Arg::new("folder")
                .num_args(1)
                .action(ArgAction::Set)
                .help("Path to the folder of files")
                .required(true),
        )
        .arg(
            Arg::new("verbose")
                .short('v')
                .long("verbose")
                .action(ArgAction::SetTrue)
                .help("Adds verbose logging")
                .conflicts_with("quiet"),
        )
        .arg(
            Arg::new("quiet")
                .short('q')
                .long("quiet")
                .action(ArgAction::SetTrue)
                .help("Removes non-error logging")
                .conflicts_with("verbose"),
        )
        .arg(
            Arg::new("logfile")
                .short('l')
                .long("logfile")
                .action(ArgAction::SetTrue)
                .help("Save logfile [rename.log]"),
        )
        .arg(
            Arg::new("yes")
                .short('y')
                .long("yes")
                .action(ArgAction::SetTrue)
                .help("Automatic yes to prompts"),
        )
        .arg(
            Arg::new("nro")
                .short('n')
                .long("nro")
                .action(ArgAction::Set)
                .value_parser(value_parser!(usize))
                .help("Define custom number to start counting from"),
        )
        .arg(
            Arg::new("zeroes")
                .short('z')
                .long("zeroes")
                .action(ArgAction::Set)
                .value_parser(value_parser!(usize))
                .help("Define custom number of zeroes before number"),
        )
        .arg(
            Arg::new("prefix")
                .short('p')
                .long("prefix")
                .action(ArgAction::Set)
                .help("Define prefix"),
        )
        .arg(
            Arg::new("mode")
                .short('m')
                .long("mode")
                .action(ArgAction::Set)
                .value_parser([
                    PossibleValue::new("n").help("normal (default)"),
                    PossibleValue::new("a").help("skip missing and add new files to the end"),
                    PossibleValue::new("f").help("fixing missing numbers"),
                ])
                .help("Choose what renaming mode to use"),
        )
        .arg(
            Arg::new("ordering")
                .short('o')
                .long("order")
                .action(ArgAction::Set)
                .value_parser([
                    PossibleValue::new("a").help("A-Z (default)"),
                    PossibleValue::new("z").help("Z-A"),
                    PossibleValue::new("n").help("Newest to oldest"),
                    PossibleValue::new("o").help("Oldest to newest"),
                ])
                .help("Choose ordering for the files"),
        )
        .arg(
            Arg::new("ignore")
                .long("ignore")
                .action(ArgAction::Append)
                .value_parser(value_parser!(usize))
                .help("Give a list of missing numbers to ignore"),
        )
        .get_matches();

    let log_level = if matches.get_flag("verbose") {
        LogLevel::Verbose
    } else if matches.get_flag("quiet") {
        LogLevel::Quiet
    } else {
        LogLevel::Standard
    };

    refi::rename(matches.into(), log_level);
}

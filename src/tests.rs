use super::*;
use lazy_regex::regex;
use std::fs::{create_dir, remove_dir_all};
use vfs::{PhysicalFS, VfsPath};

#[test]
fn test_mode_0_with_nothing_to_rename() {
    let test_path = "/tmp/test01/";
    let root = default_state(test_path);

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000006.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000007.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 6);
}

#[test]
fn test_mode_0_with_all_to_rename() {
    let test_path = "/tmp/test02/";
    let root = default_messy_state(test_path);

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000007.png").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 6);
}

#[test]
fn test_mode_0_with_confusing() {
    let test_path = "/tmp/test03/";
    let root = default_confusing_state(test_path);

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000006.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000007.png").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 6);
}

#[test]
fn test_mode_0_with_custom_start() {
    let test_path = "/tmp/test04/";
    let root = default_state(test_path);

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 3,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(!root.join("0000001.png").unwrap().exists().unwrap());
    assert!(!root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.png").unwrap().exists().unwrap());
    assert!(root.join("0000007.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000008.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000009.png").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 6);
}

#[test]
fn test_mode_0_with_custom_zeroes() {
    let test_path = "/tmp/test05/";
    let root = default_state(test_path);

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 3,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{3})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    // Default zeroes
    assert!(!root.join("0000001.png").unwrap().exists().unwrap());
    assert!(!root.join("0000002.png").unwrap().exists().unwrap());
    assert!(!root.join("0000003.png").unwrap().exists().unwrap());
    assert!(!root.join("0000004.png").unwrap().exists().unwrap());
    assert!(!root.join("0000005.png").unwrap().exists().unwrap());
    assert!(!root.join("0000006.png").unwrap().exists().unwrap());
    // 3 zeroes
    assert!(root.join("001.png").unwrap().exists().unwrap());
    assert!(root.join("002.png").unwrap().exists().unwrap());
    assert!(root.join("003.png").unwrap().exists().unwrap());
    assert!(root.join("004.png").unwrap().exists().unwrap());
    assert!(root.join("005.mp3").unwrap().exists().unwrap());
    assert!(root.join("006.txt").unwrap().exists().unwrap());
    assert!(!root.join("007.png").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 6);
}

#[test]
fn test_mode_0_with_1_missing() {
    let test_path = "/tmp/test06/";
    let root = default_state(test_path);

    // Rename file for testing
    if let Err(e) = root
        .join("0000001.png")
        .unwrap()
        .move_file(&root.join("0000005.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    // Default zeroes
    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000006.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000007.png").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 6);
}

#[test]
fn test_mode_0_with_2_missing() {
    let test_path = "/tmp/test07/";
    let root = default_state(test_path);

    // Rename files for testing
    if let Err(e) = root
        .join("0000002.png")
        .unwrap()
        .move_file(&root.join("0000005.png").unwrap())
    {
        eprintln!("{}", e);
    };

    if let Err(e) = root
        .join("0000003.png")
        .unwrap()
        .move_file(&root.join("0000006.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    // Default zeroes
    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000006.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000007.png").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 6);
}

#[test]
fn test_mode_0_with_new() {
    let test_path = "/tmp/test08/";
    let root = default_state(test_path);

    // Create new file for testing
    if let Err(e) = root.join("new.png").unwrap().create_file() {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000007.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000008.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 7);
}

#[test]
fn test_mode_0_with_missing_and_new() {
    let test_path = "/tmp/test09/";
    let root = default_state(test_path);

    // Create new file for testing
    if let Err(e) = root.join("new.png").unwrap().create_file() {
        eprintln!("{}", e);
    };

    // Rename file for testing
    if let Err(e) = root
        .join("0000002.png")
        .unwrap()
        .move_file(&root.join("0000005.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000007.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000008.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 7);
}

#[test]
fn test_mode_0_with_logfile() {
    let test_path = "/tmp/test10/";
    let root = default_state(test_path);

    // Create new file for testing
    if let Err(e) = root.join("new.png").unwrap().create_file() {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: true,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000007.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000008.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert!(root.join("rename.log").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 8);

    // Check that the logfile contains right text
    assert!(root
        .join("rename.log")
        .unwrap()
        .read_to_string()
        .unwrap()
        .contains("new.png -> 0000005.png"));
}

#[test]
fn test_mode_1_with_nothing_to_rename() {
    let test_path = "/tmp/test11/";
    let root = default_state(test_path);

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "f".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.png").unwrap().exists().unwrap());
    assert!(!root.join("0000007.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 6);
}

#[test]
fn test_mode_1_with_1_missing() {
    let test_path = "/tmp/test12/";
    let root = default_state(test_path);

    // Remove 2 files for testing
    if let Err(e) = root.join("song.mp3").unwrap().remove_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("textfile.txt").unwrap().remove_file() {
        eprintln!("{}", e);
    };

    // Rename file for testing
    if let Err(e) = root
        .join("0000001.png")
        .unwrap()
        .move_file(&root.join("0000005.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "f".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(!root.join("0000005.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 4);
}

#[test]
fn test_mode_1_with_2_missing() {
    let test_path = "/tmp/test13/";
    let root = default_state(test_path);

    // Remove 2 files for testing
    if let Err(e) = root.join("song.mp3").unwrap().remove_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("textfile.txt").unwrap().remove_file() {
        eprintln!("{}", e);
    };

    // Rename files for testing
    if let Err(e) = root
        .join("0000002.png")
        .unwrap()
        .move_file(&root.join("0000005.png").unwrap())
    {
        eprintln!("{}", e);
    };

    if let Err(e) = root
        .join("0000003.png")
        .unwrap()
        .move_file(&root.join("0000006.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "f".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(!root.join("0000005.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 4);
}

#[test]
fn test_mode_1_with_new() {
    let test_path = "/tmp/test14/";
    let root = default_state(test_path);

    // Remove 2 files for testing
    if let Err(e) = root.join("song.mp3").unwrap().remove_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("textfile.txt").unwrap().remove_file() {
        eprintln!("{}", e);
    };

    // Create new file for testing
    if let Err(e) = root.join("0000006.png").unwrap().create_file() {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "f".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(!root.join("0000006.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 5);
}

#[test]
fn test_mode_1_with_logfile() {
    let test_path = "/tmp/test16/";
    let root = default_state(test_path);

    // Remove 2 files for testing
    if let Err(e) = root.join("song.mp3").unwrap().remove_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("textfile.txt").unwrap().remove_file() {
        eprintln!("{}", e);
    };

    // Create new file for testing
    if let Err(e) = root.join("0000006.png").unwrap().create_file() {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: true,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "f".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(!root.join("0000006.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert!(root.join("rename.log").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 6);

    // Check that the logfile contains right text
    assert!(root
        .join("rename.log")
        .unwrap()
        .read_to_string()
        .unwrap()
        .contains("0000006.png -> 0000005.png"));
}

#[test]
fn test_mode_1_with_confusing() {
    let test_path = "/tmp/test17/";
    let root = default_confusing_state(test_path);

    // Remove 2 files for testing
    if let Err(e) = root.join("song.mp3").unwrap().remove_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("textfile.txt").unwrap().remove_file() {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(!root.join("0000005.mp3").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 4);
}

#[test]
fn test_mode_1_with_custom_start() {
    let test_path = "/tmp/test18/";
    let root = default_state(test_path);

    // Remove 2 files for testing
    if let Err(e) = root.join("song.mp3").unwrap().remove_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("textfile.txt").unwrap().remove_file() {
        eprintln!("{}", e);
    };

    // Create new file for testing
    if let Err(e) = root.join("0000006.png").unwrap().create_file() {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 3,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "f".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(!root.join("0000001.png").unwrap().exists().unwrap());
    assert!(!root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.png").unwrap().exists().unwrap());
    assert!(root.join("0000007.png").unwrap().exists().unwrap());
    assert!(!root.join("0000008.png").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 5);
}

#[test]
fn test_mode_1_with_custom_zeroes() {
    let test_path = "/tmp/test19/";
    let root = default_state(test_path);
    // Remove 2 files for testing
    if let Err(e) = root.join("song.mp3").unwrap().remove_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("textfile.txt").unwrap().remove_file() {
        eprintln!("{}", e);
    };

    // Rename files for testing
    if let Err(e) = root
        .join("0000001.png")
        .unwrap()
        .move_file(&root.join("001.png").unwrap())
    {
        eprintln!("{}", e);
    };
    if let Err(e) = root
        .join("0000002.png")
        .unwrap()
        .move_file(&root.join("005.png").unwrap())
    {
        eprintln!("{}", e);
    };
    if let Err(e) = root
        .join("0000003.png")
        .unwrap()
        .move_file(&root.join("003.png").unwrap())
    {
        eprintln!("{}", e);
    };
    if let Err(e) = root
        .join("0000004.png")
        .unwrap()
        .move_file(&root.join("004.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 3,
        prefix: "".to_string(),
        mode: "f".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{3})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    // Default zeroes
    assert!(!root.join("0000001.png").unwrap().exists().unwrap());
    assert!(!root.join("0000002.png").unwrap().exists().unwrap());
    assert!(!root.join("0000003.png").unwrap().exists().unwrap());
    assert!(!root.join("0000004.png").unwrap().exists().unwrap());
    assert!(!root.join("0000005.png").unwrap().exists().unwrap());
    // 3 zeroes
    assert!(root.join("001.png").unwrap().exists().unwrap());
    assert!(root.join("002.png").unwrap().exists().unwrap());
    assert!(root.join("003.png").unwrap().exists().unwrap());
    assert!(root.join("004.png").unwrap().exists().unwrap());
    assert!(!root.join("005.png").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 4);
}
#[test]
fn test_ordering_with_z_mode_0_nothing_to_rename() {
    let test_path = "/tmp/test20/";
    let root = time_state_default(test_path);

    // Remove 2 files for testing
    if let Err(e) = root.join("song.mp3").unwrap().remove_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("textfile.txt").unwrap().remove_file() {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: true,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "0".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(!root.join("0000005.txt").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert!(!root.join("logfile.log").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 4);
}
#[test]
fn test_ordering_with_z_mode_0() {
    let test_path = "/tmp/test21/";
    let root = time_state_default(test_path);

    // Rename files for testing
    if let Err(e) = root
        .join("0000004.png")
        .unwrap()
        .move_file(&root.join("aa.png").unwrap())
    {
        eprintln!("{}", e);
    };
    if let Err(e) = root
        .join("0000003.png")
        .unwrap()
        .move_file(&root.join("bb.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: true,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "0".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.txt").unwrap().exists().unwrap());
    assert!(root.join("0000004.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.png").unwrap().exists().unwrap());
    assert!(!root.join("0000007.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 7);

    // Check that the logfile contains right text
    assert!(root
        .join("rename.log")
        .unwrap()
        .read_to_string()
        .unwrap()
        .contains("textfile.txt -> 0000003.txt\nsong.mp3 -> 0000004.mp3\nbb.png -> 0000005.png\naa.png -> 0000006.png"));
}
#[test]
fn test_ordering_with_n_mode_0() {
    let test_path = "/tmp/test22/";
    let root = time_state_default(test_path);

    // Rename files for testing
    if let Err(e) = root
        .join("0000004.png")
        .unwrap()
        .move_file(&root.join("aa.png").unwrap())
    {
        eprintln!("{}", e);
    };
    if let Err(e) = root
        .join("0000003.png")
        .unwrap()
        .move_file(&root.join("bb.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: true,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000006.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000007.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 7);

    // Check that the logfile contains right text
    assert!(root
        .join("rename.log")
        .unwrap()
        .read_to_string()
        .unwrap()
        .contains("bb.png -> 0000003.png\naa.png -> 0000004.png"));
}
#[test]
fn test_ordering_with_o_mode_0() {
    let test_path = "/tmp/test23/";
    let root = time_state_default(test_path);

    // Rename files for testing
    if let Err(e) = root
        .join("0000004.png")
        .unwrap()
        .move_file(&root.join("aa.png").unwrap())
    {
        eprintln!("{}", e);
    };
    if let Err(e) = root
        .join("0000003.png")
        .unwrap()
        .move_file(&root.join("bb.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: true,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "o".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000004.txt").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.png").unwrap().exists().unwrap());
    assert!(!root.join("0000007.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 7);

    // Check that the logfile contains right text
    assert!(root
        .join("rename.log")
        .unwrap()
        .read_to_string()
        .unwrap()
        .contains("song.mp3 -> 0000003.mp3\ntextfile.txt -> 0000004.txt\naa.png -> 0000005.png\nbb.png -> 0000006.png"));
}
#[test]
fn test_ordering_with_z_mode_1_nothing_to_rename() {
    let test_path = "/tmp/test24/";
    let root = time_state_default(test_path);

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "f".to_string(),
        ordering: "0".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.png").unwrap().exists().unwrap());
    assert!(!root.join("0000007.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 8);
}
#[test]
fn test_ordering_with_z_mode_1() {
    let test_path = "/tmp/test25/";
    let root = time_state_default(test_path);

    // Remove 2 files for testing
    if let Err(e) = root.join("song.mp3").unwrap().remove_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("textfile.txt").unwrap().remove_file() {
        eprintln!("{}", e);
    };

    // Rename files for testing
    if let Err(e) = root
        .join("0000002.png")
        .unwrap()
        .move_file(&root.join("0000006.png").unwrap())
    {
        eprintln!("{}", e);
    };
    if let Err(e) = root
        .join("0000004.png")
        .unwrap()
        .move_file(&root.join("0000007.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: true,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "f".to_string(),
        ordering: "z".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.png").unwrap().exists().unwrap());
    assert!(!root.join("0000007.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 7);

    // Check that the logfile contains right text
    assert!(root
        .join("rename.log")
        .unwrap()
        .read_to_string()
        .unwrap()
        .contains("bb.png -> 0000003.png\naa.png -> 0000004.png"));
}
#[test]
fn test_ordering_with_n_mode_1() {
    let test_path = "/tmp/test26/";
    let root = time_state_default(test_path);

    // Remove 2 files for testing
    if let Err(e) = root.join("song.mp3").unwrap().remove_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("textfile.txt").unwrap().remove_file() {
        eprintln!("{}", e);
    };

    // Rename files for testing
    if let Err(e) = root
        .join("0000004.png")
        .unwrap()
        .move_file(&root.join("0000006.png").unwrap())
    {
        eprintln!("{}", e);
    };
    if let Err(e) = root
        .join("0000003.png")
        .unwrap()
        .move_file(&root.join("0000007.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: true,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "f".to_string(),
        ordering: "n".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(!root.join("0000005.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 5);

    // Check that the logfile contains right text
    if cfg!(target_os = "linux") {
        // This is expected result on linux
        assert!(root
            .join("rename.log")
            .unwrap()
            .read_to_string()
            .unwrap()
            .contains("0000007.png -> 0000003.png\n0000006.png -> 0000004.png"));
    } else {
        // This is the expected result on macos
        assert!(root
            .join("rename.log")
            .unwrap()
            .read_to_string()
            .unwrap()
            .contains("0000006.png -> 0000003.png\n0000007.png -> 0000004.png"));
    }
}
#[test]
fn test_ordering_with_o_mode_1() {
    let test_path = "/tmp/test27/";
    let root = time_state_default(test_path);

    // Remove 2 files for testing
    if let Err(e) = root.join("song.mp3").unwrap().remove_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("textfile.txt").unwrap().remove_file() {
        eprintln!("{}", e);
    };

    // Rename files for testing
    if let Err(e) = root
        .join("0000004.png")
        .unwrap()
        .move_file(&root.join("0000006.png").unwrap())
    {
        eprintln!("{}", e);
    };
    if let Err(e) = root
        .join("0000003.png")
        .unwrap()
        .move_file(&root.join("0000007.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: true,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "f".to_string(),
        ordering: "o".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(!root.join("0000005.png").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 5);

    // Check that the logfile contains right text
    assert!(root
        .join("rename.log")
        .unwrap()
        .read_to_string()
        .unwrap()
        .contains("0000006.png -> 0000003.png\n0000007.png -> 0000004.png"));
}

#[test]
fn test_ignore_with_1_missing() {
    let test_path = "/tmp/test28/";
    let root = default_state(test_path);

    // Rename file for testing
    if let Err(e) = root
        .join("0000004.png")
        .unwrap()
        .move_file(&root.join("0000005.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![4],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    // Default zeroes
    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(!root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000007.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000008.png").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 6);
}

#[test]
fn test_ignore_with_multiple_missing() {
    let test_path = "/tmp/test29/";
    let root = default_state(test_path);

    // Rename file for testing
    if let Err(e) = root
        .join("0000003.png")
        .unwrap()
        .move_file(&root.join("0000006.png").unwrap())
    {
        eprintln!("{}", e);
    };
    if let Err(e) = root
        .join("0000004.png")
        .unwrap()
        .move_file(&root.join("aa.png").unwrap())
    {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "n".to_string(),
        ordering: "a".to_string(),
        ignore: vec![4, 3],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    // Default zeroes
    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(!root.join("0000003.png").unwrap().exists().unwrap());
    assert!(!root.join("0000004.png").unwrap().exists().unwrap());
    assert!(root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.png").unwrap().exists().unwrap());
    assert!(root.join("0000007.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000008.txt").unwrap().exists().unwrap());
    assert!(!root.join("0000009.png").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 6);
}

#[test]
fn test_mode_2_with_new() {
    let test_path = "/tmp/test30/";
    let root = default_state(test_path);

    // Create new files for testing
    if let Err(e) = root.join("0000006.png").unwrap().create_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("abc.png").unwrap().create_file() {
        eprintln!("{}", e);
    };
    if let Err(e) = root.join("00000123.png").unwrap().create_file() {
        eprintln!("{}", e);
    };

    let args = Arguments {
        path: Path::new(test_path).to_owned(),
        logfile: false,
        yes: true,
        nro: 1,
        zeroes: 7,
        prefix: "".to_string(),
        mode: "a".to_string(),
        ordering: "a".to_string(),
        ignore: vec![],
        file_extension: regex!(r"(?i)\.[0-9A-Z]+$"),
        file_numbers: Regex::new(r"(?i)(^\d{7})\.[0-9A-Z]+$").unwrap(),
    };

    rename(args, LogLevel::Standard);

    assert!(root.join("0000001.png").unwrap().exists().unwrap());
    assert!(root.join("0000002.png").unwrap().exists().unwrap());
    assert!(root.join("0000003.png").unwrap().exists().unwrap());
    assert!(root.join("0000004.png").unwrap().exists().unwrap());
    assert!(!root.join("0000005.png").unwrap().exists().unwrap());
    assert!(root.join("0000006.png").unwrap().exists().unwrap());
    assert!(root.join("0000007.png").unwrap().exists().unwrap());
    assert!(root.join("0000008.png").unwrap().exists().unwrap());
    assert!(root.join("0000009.mp3").unwrap().exists().unwrap());
    assert!(root.join("0000010.txt").unwrap().exists().unwrap());
    assert!(!root.join("song.mp3").unwrap().exists().unwrap());
    assert!(!root.join("textfile.txt").unwrap().exists().unwrap());
    assert_eq!(root.read_dir().unwrap().count(), 8);
}

fn default_state(test_path: &str) -> VfsPath {
    // Check if the temp dir exists and delete it with all of it's contents
    if Path::new(test_path).exists() {
        if let Err(e) = remove_dir_all(test_path) {
            panic!("{}", e);
        };
    }

    // Create empty dir for testing
    if let Err(e) = create_dir(test_path) {
        panic!("{}", e);
    };

    let root: VfsPath = PhysicalFS::new(Path::new(test_path)).into();

    if let Err(e) = root.join("0000001.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("0000002.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("0000003.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("0000004.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("song.mp3").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("textfile.txt").unwrap().create_file() {
        eprintln!("{}", e);
    }

    root
}

fn default_confusing_state(test_path: &str) -> VfsPath {
    // Check if the temp dir exists and delete it with all of it's contents
    if Path::new(test_path).exists() {
        if let Err(e) = remove_dir_all(test_path) {
            panic!("{}", e);
        };
    }

    // Create empty dir for testing
    if let Err(e) = create_dir(test_path) {
        panic!("{}", e);
    };

    let root: VfsPath = PhysicalFS::new(Path::new(test_path)).into();

    if let Err(e) = root.join("0000001.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("0000002.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("0000003.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("004.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("song.mp3").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("textfile.txt").unwrap().create_file() {
        eprintln!("{}", e);
    }

    root
}

fn default_messy_state(test_path: &str) -> VfsPath {
    // Check if the temp dir exists and delete it with all of it's contents
    if Path::new(test_path).exists() {
        if let Err(e) = remove_dir_all(test_path) {
            panic!("{}", e);
        };
    }

    // Create empty dir for testing
    if let Err(e) = create_dir(test_path) {
        panic!("{}", e);
    };

    let root: VfsPath = PhysicalFS::new(Path::new(test_path)).into();

    if let Err(e) = root.join("dslajfhkjh.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("09214dhsakjkdh.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("djksahfk21.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root
        .join("2198347912798sajdhkashdkh.png")
        .unwrap()
        .create_file()
    {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("dhaskjfhkas769879.mp3").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("sdafhkjsh7987070.txt").unwrap().create_file() {
        eprintln!("{}", e);
    }

    root
}
fn time_state_default(test_path: &str) -> VfsPath {
    // Check if the temp dir exists and delete it with all of it's contents
    if Path::new(test_path).exists() {
        if let Err(e) = remove_dir_all(test_path) {
            panic!("{}", e);
        };
    }

    // Create empty dir for testing
    if let Err(e) = create_dir(test_path) {
        panic!("{}", e);
    };

    let root: VfsPath = PhysicalFS::new(Path::new(test_path)).into();

    if let Err(e) = root.join("0000003.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("0000002.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("0000004.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("0000001.png").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("song.mp3").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join("textfile.txt").unwrap().create_file() {
        eprintln!("{}", e);
    }
    if let Err(e) = root.join(".hidden.txt").unwrap().create_file() {
        eprintln!("{}", e);
    }

    root
}
